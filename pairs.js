// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(obj) {
  if (typeof obj !== "object" || obj === null || Array.isArray(obj)) {
    //to check if argument is valid object or not
    console.log("Invalid object");
    return;
  }

  let listArray = [];
  for (let key in obj) {
    listArray.push([key, obj[key]]); //to push key value pairs in a new array
  }
  return listArray;
}

module.exports = pairs; //Exporting pairs function to its test file
