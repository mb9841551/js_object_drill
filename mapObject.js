// Like map for arrays, but for objects.
// Transform the value of each property in turn by passing it to the callback function.

function mapObjects(obj, cb) {
  if (typeof obj !== "object" || obj === null || Array.isArray(obj)) {
    //to check if argument is valid object or not
    console.log("Invalid object");
    return;
  }

  let ansObject = {};

  for (let key in obj) {
    ansObject[key] = cb(obj[key]); //to assign the key and value pairs in ansObject using callback function
  }
  return ansObject;
}

module.exports = mapObjects; //Exporting mapObjects function to its test file
