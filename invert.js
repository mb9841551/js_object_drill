// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

function invert(obj) {
  if (typeof obj !== "object" || obj === null || Array.isArray(obj)) {
    //to check if argument is valid object or not
    console.log("Invalid object");
    return;
  }
  let ansObj = {};
  for (let keys in obj) {
    ansObj[obj[keys]] = keys; //to interchange the values of key and its value in an new object
  }
  return ansObj;
}

module.exports = invert; //Exporting invert function to its test file
