// Return all of the values of the object's own properties.
// Ignore functions

function values(obj){
    if (typeof obj !== 'object' || obj === null || Array.isArray(obj)) {             //to check if argument is valid object or not
        console.log("Invalid object");
        return ;
    }
    let stringArray = [];
    for(let key in obj){
        stringArray.push(obj[key]);             //to push keys to a new array 
    }
    return stringArray;
}

module.exports = values;                     //Exporting values function to its test file
