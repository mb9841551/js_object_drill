// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.

function defaults(obj, defaultProps) {
  if (
    typeof obj !== "object" ||
    obj === null ||
    Array.isArray(obj) ||
    typeof defaultProps !== "object" ||
    defaultProps === null ||
    Array.isArray(defaultProps)
  ) {
    //to check if arguments are invalid object or not
    console.log("Invalid object");
    return;
  }

  for (let keys in obj) {
    if (obj[keys] === undefined) {
      //to check if any object have any key's value which is undefined
      obj[keys] = defaultProps[keys]; //assigning undefined value from defaultProps object
    }
  }
  return obj;
}

module.exports = defaults; //Exporting defaults finction to its test file
