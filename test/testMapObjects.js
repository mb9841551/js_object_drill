const problemMapObjects = require("../mapObject.js"); //Importing mapObjects.js file to problemMapObjects

let items = {
  rollNo1: 11,
  rollNo2: 12,
  rollNo3: 13,
};

function callback(objValue) {
  return objValue * 2;
}

const ansObject = problemMapObjects(items, callback);

console.log(ansObject);
