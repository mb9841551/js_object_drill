// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.

function keys(obj) {
  if (typeof obj !== "object" || obj === null || Array.isArray(obj)) {
    //to check if argument is valid object or not
    console.log("Invalid object");
    return;
  }
  let stringArray = [];
  for (let key in obj) {
    stringArray.push(key); //to push object's keys to a new array
  }
  return stringArray;
}

module.exports = keys; //Exporting keys function to its test file
