const problemDefaults = require("../defaults.js"); //Importing defaults.js file to problemDefaults

const item1 = {
  //new object created
  rollNo1: 15,
  rollNo2: 16,
  rollNo3: undefined,
};

const item2 = {
  //new object created
  rollNo1: 15,
  rollNo2: 16,
  rollNo3: 17,
};

const ansObject = problemDefaults(item1, item2); //to call the function

console.log(ansObject);
